/* 2. Generate a Report showing by year: total, max, min, and average salary
    * Exclude Managers from Report
    * Managers are defined in the table `DEPT_MANAGER`
    * Don’t worry about active dates at this time.
    * Hint use NOT IN clause
*/

select
	DATE_FORMAT(from_date, '%Y') as salary_year, 
    sum(salary) as total_salary,
    max(salary) as max_salary, 
    min(salary) as min_salary,
    avg(salary) as average_salary from salaries S 
where 
	S.emp_no not in (select emp_no from dept_manager)    
group by salary_year; 
    
    