/* 
Generate Orders Report:

Download northwind database dump from: https://gitlab.com/endrit-b/test_db
Import data using MySQL Workbench, by running sql script northwind.sql
Show Customer First Name, Last Name
Shipping Company Name
Order Date (formatted as January, 1, 2018), Shipping Address (street, city, state, zip, country)
Product Code, Product Name, List Price, quantity ordered, and total cost of line item
Provide friendly column names
Format numbers to have commas and limit decimals to two places. 
*/

select 
	c.first_name as Firstname, c.last_name as Lastname, /* customer firstname and lastname */
	o.ship_name as ShipName, /* shipping company name */
	o.order_date as OrderDate, Concat(o.ship_address, " ", o.ship_city, " ", o.ship_state_province, " ", ship_country_region) as Address, /* order date and shipping address */
	p.product_code as ProductCode, p.product_name as ProductName, /* product code and name */
	Cast(p.list_price as Decimal(10,2)) as ListPrice, Cast(od.quantity as Decimal(10,2)) as Quantity /* list_price, quantity and -LINE ITEM- as two decimal point numbers*/
from 
	orders o, order_details od, customers c, products p 
where p.id = od.product_id and o.id = od.id and o.customer_id = c.id; /* inner joins */

/*
Generate monthly profit report by item:

Report aggregated by year, by month, per item
Show total sales, cost, and profit 
List Price order quantity - standard cost order quantity
Limit to order lines invoiced
Show total revenue - List Price * order quantity
Limit to order lines invoiced
*/

select 
	count(*) as TotalSales, /* total sales */
    unit_price as Cost,
    sum(unit_price) as Profit, /* cost */
    p.list_price * od.quantity as Revenue, /* List Price * Order Quantity */
    p.list_price as ListPrice, /* order quantity */
    p.standard_cost as StandardCost, /* standard cost price */
    date_format(o.order_date, '%Y') as `Year`, 
    date_format(o.order_date, '%M') as `Date` from orders o, order_details od, products p 
where 
	o.id = od.product_id and p.id = od.product_id /* inner join */
group by 
	year, date, od.product_id /* report aggregated by year, month per item */
order by 
	ListPrice desc, StandardCost desc; /* order by listprice quantity, standardcost quantity */
    
/*
[BONUS] Generate Weekly Sales Report per Employee:

Report should list each employee and show zero if the employee had no sales
Should be values for each week company had sales
Use outer joins
Use ifnull function to provide zero values
Hint - you will need to use a subquery for order data
*/
/* two views, one with non empty values, one with empty values */
create or replace view `non_empty` as
	select o.employee_id as `id`, count(*) as Nr, date_format(o.order_date, '%W') as `Week` from orders o group by o.employee_id;

create or replace view `empty` as
	select e.id, null, null from employees e where e.id not in (select t.id from temp t);
    
/* if sales is null, replace with 0, if Week is null, replace it with None */
select id, ifnull(Nr, 0), ifnull(`Week`, 'None') from 
(    
select * from `non_empty`
union
select * from `empty`
) m;
