package io.goprime.training.SparkRDD;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import java.util.Arrays;

public class LocatedInUSA {
    public static void main (String[] args) {
        // this reduced the amount of errors outputted
        Logger.getLogger("org").setLevel(Level.ERROR);
        // App name to 'Located in Usa' with 3 processing cores assigned
        SparkConf conf = new SparkConf().setAppName("Located In USA").setMaster("local[3]");
        JavaSparkContext sc = new JavaSparkContext(conf);

        // loading airports.txt from the directory
        JavaRDD<String> path = sc.textFile("src/main/resources/airports.txt");
        // finding every row whose third after comma name is 'United States'
        JavaRDD<String> words = path.filter(line -> line.split(",")[3].equals("United States"));
        // map each data as a row
        JavaRDD<String> getting = words.flatMap(line -> Arrays.asList(line.split("\n")).iterator());

        // for each row, output second and third array items
        getting.foreach(x ->
                System.out.println(x.split(",")[1] + ", " + x.split(",")[2])
        );
    }
}
