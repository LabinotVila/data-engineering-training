package io.goprime.training.SparkRDD;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import java.util.Arrays;
import java.util.List;

public class BothDays {
    public static void main(String[] args) {
        System.setProperty("hadoop.home.dir", "C:\\hadoop");
        Logger.getLogger("org").setLevel(Level.ERROR);
        SparkConf conf = new SparkConf().setAppName("BOTH days").setMaster("local[2]");
        JavaSparkContext sc = new JavaSparkContext(conf);

        // get only hosts of first file
        JavaRDD<String> firstFile = sc.textFile("src/main/resources/nasa_19950701.tsv");
        JavaRDD<String> firstAsArray = firstFile.flatMap(val -> Arrays.asList(val.split("\n")).iterator())
                .map(v -> v.replaceAll("\\s{2,}", " "))
                .flatMap(v -> Arrays.asList(v.split("\\s")[0]).iterator());

        // get only hosts of second file
        JavaRDD<String> secondFile = sc.textFile("src/main/resources/nasa_19950701.tsv");
        JavaRDD<String> secondAsArray = secondFile.flatMap(val -> Arrays.asList(val.split("\n")).iterator())
                .map(v -> v.replaceAll("\\s{2,}", " "))
                .flatMap(v -> Arrays.asList(v.split("\\s")[0]).iterator());

        // intersect them and remove 'host'
        JavaRDD<String> finalList = firstAsArray.intersection(secondAsArray);
        JavaRDD<String> filtered = finalList.filter(val -> !val.equals("host"));

        // export and print the result
        filtered.saveAsTextFile("solution/nasa_logs_same_hosts.csv");

        filtered.foreach(x -> System.out.println(x));
    }
}
