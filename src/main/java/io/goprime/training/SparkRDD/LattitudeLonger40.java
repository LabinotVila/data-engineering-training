package io.goprime.training.SparkRDD;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import java.util.Arrays;

public class LattitudeLonger40 {
    public static void main (String[] args) {
        /*
        Create a Spark program to read the airport data from in/airports.text, find all the airports whose latitude
        are bigger than 40. Then output the airport's name and the airport's latitude to
        solution/airports_by_latitude.TXT.
         */
        Logger.getLogger("org").setLevel(Level.ERROR);
        SparkConf conf = new SparkConf().setAppName("Lattitude Longer Than 40").setMaster("local[3]");
        JavaSparkContext sc = new JavaSparkContext(conf);

        // loading file
        JavaRDD<String> path = sc.textFile("src/main/resources/airports.txt");

        // split on new lines
        JavaRDD<String> lines = path.flatMap(line -> Arrays.asList(line.split("\n")).iterator());

        // check if sixth array [latitude] is number, because some rows do not have numbers
        JavaRDD<String> validOnes = lines.filter(line -> line.split(",")[6].matches(".*\\d.*"));

        // get values whose latitude is longer than 40, by converting it to double
        JavaRDD<String> yaay = validOnes.filter(list -> Double.parseDouble(list.split(",")[6]) > 40);

        // print city and latitude
        yaay.foreach(
                col -> System.out.println(col.split(",")[1] + ", " + col.split(",")[6])
        );

    }
}
