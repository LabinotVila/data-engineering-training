package io.goprime.training.SparkRDD;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import java.util.Arrays;
import java.util.List;

public class PrimeNumbers {
    public static void main(String[] args) {

        /* Logic: since the file is made of 10 rows (not one single row), first we had to define a rule where a row
        ends (\n) and then, with reduce function, we could add rows on top of each other, so we could have one single
        long row with all prime numbers. Then, since there are multiple spaces in-between each row, we had to hold
        to one single space. Then, we splitted the long row on whitespaces, parallellized it, reduced it and got a
        result.
         */

        Logger.getLogger("org").setLevel(Level.ERROR);
        SparkConf conf = new SparkConf().setAppName("WordCountsApp").setMaster("local[3]");
        JavaSparkContext sc = new JavaSparkContext(conf);

        JavaRDD<String> file = sc.textFile("src/main/resources/prime_nums.txt");
        // split on rows
        JavaRDD<String> asArray = file.flatMap(value -> Arrays.asList(value.split("\n")).iterator());

        // put rows on top of each other (one single long row)
        String reducer = asArray.reduce((x, y) -> x + " " + y);
        // replace every n-spaces with one single -> {'   ' -> ' '}
        reducer = reducer.replaceAll("\\s{2,}", " ");

        // split on whitespaces
        List<String> list = Arrays.asList(reducer.split("\\s+"));

        // parallelize splitted stuff
        JavaRDD<String> paralel = sc.parallelize(list);

        // convert to int so we can do mathematical operations
        JavaRDD<Integer> toInt = paralel.map(value -> Integer.valueOf(value));

        // sum it's reduce
        int sum = toInt.reduce((x, y) -> x+y);

        // print the result
        System.out.println("The sum of prime numbers: " + sum);
    }
}
