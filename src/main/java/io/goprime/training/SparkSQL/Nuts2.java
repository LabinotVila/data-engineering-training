package io.goprime.training.SparkSQL;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import static org.apache.spark.sql.functions.col;

public class Nuts2 {
    public static void main(String[] args) {
        SparkSession spark = SparkSession.builder()
                .appName("sqlTest")
                .master("local[*]")
                .config("spark.sql.warehouse.dir", "file:///tmp/")
                .getOrCreate();

        // dataset importation
        Dataset<Row> Q1 = spark.read()
                .option("header", true)
                .csv("nuts-sets/2016Q1_verkoop_data_winkel_selectie_noten.csv");
        Dataset<Row> Q2 = spark.read()
                .option("header", true)
                .csv("nuts-sets/2016Q2_verkoop_data_winkel_selectie_noten.csv");
        Dataset<Row> Q3 = spark.read()
                .option("header", true)
                .csv("nuts-sets/2016Q3_verkoop_data_winkel_selectie_noten.csv");
        Dataset<Row> Q4 = spark.read()
                .option("header", true)
                .csv("nuts-sets/2016Q4_verkoop_data_winkel_selectie_noten.csv");
        Dataset<Row> StoreData = spark.read()
                .option("header", true)
                .csv("nuts-sets/Store_Data.csv");
        Dataset<Row> articleData = spark.read()
                .option("header", true)
                .csv("nuts-sets/Article_Data.csv");
        Dataset<Row> nutsInfo = spark.read()
                .option("header", true)
                .csv("nuts-sets/Nuts_Information.csv");

        Dataset<Row> full = Q1.union(Q2).union(Q3).union(Q4);
        full.createOrReplaceTempView("Entries");

        Dataset<Row> replaced = spark.sql("select *, cast(dim_winkel_id as int) as `shop_id` from Entries");
        // dim_winkel_id on export transactions is double, while in Store_Data.csv is integer, join cannot be done
        // without casting to int
        Dataset<Row> joined = replaced
                .join(
                        StoreData,
                        replaced.col("shop_id").equalTo(StoreData.col("dim_winkel_id")),
                        "inner"
                )
                .join(
                        articleData,
                        replaced.col("dim_artikel_id").equalTo(articleData.col("dim_artikel_id")),
                        "inner"
                );
        Dataset<Row> complex = joined
                .join(
                        nutsInfo,
                        joined.col("artikel_kassa_oms").equalTo(nutsInfo.col("Artikel")),
                        "inner"
                );

        // COMPLEX represents the four tables joined together as one with one additional column called
        // shop_id, which casts `dim_winkel_id` to integer, because it is double and it can not be joined properly.


        // DATATABLE 2:
        complex.select(
                col("entries.dim_artikel_id"),
                col("winkel_segment"),
                col("product_type"),
                col("vers_voorverpakt"),
                col("verkoop_waarde_voor_afprijzing_voor_korting_incl_btw")
                        .divide(col("verkoop_waarde_na_afprijzing_voor_korting_incl_btw"))
        ); //.show();

        // DATATABLE 1:
        complex.createOrReplaceTempView("Complex");
        Dataset<Row> datatable2 = spark.sql("select *, regexp_replace(kassa_bon_registratie_datum, '[.]', '-') `transformed_date` from complex");
        datatable2.createOrReplaceTempView("Datatable2");
        Dataset<Row> transformed = spark.sql("select *, weekofyear(transformed_date) as `weekofyear` from Datatable2");
        transformed.createOrReplaceTempView("Transformed");

        Dataset<Row> finalizing = spark.sql("select " +
                "sum(verkoop_waarde_voor_afprijzing_voor_korting_incl_btw) as 'Sold', " +
                "count(*) as 'AmountExported', T.weekofyear, T.dim_artikel_id " +
                "from Transformed T group by T.weekofyear, T.dim_artikel_id"); //.show();

        // DATATABLE 3
        // can be derived from Nuts.java -> lines [109 - 116] [the same?]
    }
}
