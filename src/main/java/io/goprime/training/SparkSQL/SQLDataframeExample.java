package io.goprime.training.SparkSQL;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import static org.apache.spark.sql.functions.*;

public class SQLDataframeExample {

    public static void main(String[] args) {

        // Instatiate Spark session object
        SparkSession spark = SparkSession.builder()
                .appName("sqlTest")
                .master("local[*]")
                .config("spark.sql.warehouse.dir", "file:///tmp/")
                .getOrCreate();

        // Create dataset from log files
        Dataset<Row> dataset = spark.read()
                .option("header", true)
                .csv("src/main/resources/logs.txt");

        dataset = dataset.select(col("level"), date_format(col("datetime"), "MMMM").alias("month"));

        // count records by log level
        dataset = dataset.groupBy(col("level"), col("month")).count().orderBy(col("count"));
        dataset.show();

        spark.stop();
    }
}