package io.goprime.training.SparkSQL;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import static org.apache.spark.sql.functions.*;

public class Nuts {
    public static void main(String[] args) {
        SparkSession spark = SparkSession.builder()
                .appName("sqlTest")
                .master("local[*]")
                .config("spark.sql.warehouse.dir", "file:///tmp/")
                .getOrCreate();

        // dataset importation
        Dataset<Row> Q1 = spark.read()
                .option("header", true)
                .csv("nuts-sets/2016Q1_verkoop_data_winkel_selectie_noten.csv");
        Dataset<Row> Q2 = spark.read()
                .option("header", true)
                .csv("nuts-sets/2016Q2_verkoop_data_winkel_selectie_noten.csv");
        Dataset<Row> Q3 = spark.read()
                .option("header", true)
                .csv("nuts-sets/2016Q3_verkoop_data_winkel_selectie_noten.csv");
        Dataset<Row> Q4 = spark.read()
                .option("header", true)
                .csv("nuts-sets/2016Q4_verkoop_data_winkel_selectie_noten.csv");
        Dataset<Row> articleData = spark.read()
                .option("header", true)
                .csv("nuts-sets/Article_Data.csv");
        Dataset<Row> nutsInfo = spark.read()
                .option("header", true)
                .csv("nuts-sets/Nuts_Information.csv");

        // ----------- INSIGHT 1:
        // checking throughout the whole year the amount of each product that has been exported
        Dataset<Row> full = Q1.union(Q2).union(Q3).union(Q4);
        Dataset<Row> fullSorted = full
                .select("dim_artikel_id")
                .groupBy(col("dim_artikel_id").as("article_id"))
                .agg(count(col("*")).as("count"))
                .orderBy(col("count").desc());

        // we find the total number of unique articles we have exported throughout the year [153 is the result]
        Dataset<Row> checkUniques = full.select(countDistinct(col("dim_artikel_id"))); //.show

        // in 153 unique values, we say 40 is a good number to work with [top exported products]
        Dataset<Row> top40sorted = fullSorted.limit(40);

        // then, we can check top exported products in each quarter of the year, so we can have an insight of
        // the products that have had growth in exportation, that are not being exported much and small other details.
        Dataset<Row> whichQuarter = Q1; // can be Q1, Q2, Q3 or Q4
        Dataset<Row> quarter = whichQuarter
                .groupBy(col("dim_artikel_id").as("article_id"))
                .agg(count("*").as("count"))
                .orderBy(col("count").desc())
                .limit(40);

        // no IN in Apache Spark, therefore doing it the SQL way
        quarter.createOrReplaceTempView("Quarter");
        top40sorted.createOrReplaceTempView("TopSorted");
        Dataset<Row> stillFiltering = spark.sql("select article_id from Quarter where article_id in " +
                "(select article_id from TopSorted) group by article_id");
        // stillFiltering.show();
        // for example, product with ID 1 can appear in quarter 3 and 4, but not in quarter 1 and 2, which means that
        // that product is being exported more and more, and that's a good insight to the exporter.


        // ----------- INSIGHT 2:
        // we can provide our customer with good knowledge if we check the amount of exports a product has done and
        // multiply it with margin percentage, then we can get a good product performance
        Dataset<Row> complex = fullSorted // will be used below again
                .join(
                        articleData,
                        fullSorted.col("article_id").equalTo(articleData.col("dim_artikel_id"))
                )
                .join(
                        nutsInfo,
                        articleData.col("artikel_kassa_oms").equalTo(nutsInfo.col("artikel")));

        Dataset<Row> lastComplex = complex
                .select(col("article_id"), col("count"), col("marge_percentage"))
                .orderBy(col("count").desc(), col("marge_percentage").desc());
        // lastComplex.show();

        // for example, a very good product is one that appears on top of `complex` table, because it is being
        // exported a lot and has really good margin percentage



        // ----------- INSIGHT 3:
        // PACKAGE TYPE
        // now we have a closer look on what made the products be exported more
        complex
                .select(col("vers_voorverpakt_dummy").as("PackagedType"))
                .groupBy(col("PackagedType")).agg(count("*").as("counter")); //.show();
        // running this, we can see that there is a clear impact that exported products packaged as `2` are far more
        // required.

        // SUPPLIER
        complex
                .select(col("actieve_leverancier_code"))
                .groupBy(col("actieve_leverancier_code")).agg(count("*")); //.show();
        // products with code `Niet ingevuld` and `snoep` should obviously be increased in size, since they are being
        // exported really much too

        // BRAND
        // now we see if there are specific brands which are more popular than others and have direct impact on export
        complex
                .select(col("merk_code")).as("Brand")
                .groupBy(col("merk_code")).agg(count("*"))
                .orderBy(col("count(1)").desc()); //.show();
        // we can clearly see that brands do not matter at all in our use case [most exported brand has been exported
        // 4 times for example]

        complex
                .select(col("product_type")).as("ProductType")
                .groupBy(col("product_type")).agg(count("*")).as("Count")
                .orderBy(col("count(1)").desc()); //.show();
        // we can see that mix products are exported the most, its a good insight to our customer
    }
}