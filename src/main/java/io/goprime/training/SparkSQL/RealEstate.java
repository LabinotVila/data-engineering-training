package io.goprime.training.SparkSQL;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import static org.apache.spark.sql.functions.*;

public class RealEstate {
    public static void main(String[] args) {
        SparkSession spark = SparkSession.builder()
                .appName("sqlTest")
                .master("local[*]")
                .config("spark.sql.warehouse.dir", "file:///tmp/")
                .getOrCreate();

        Dataset<Row> dataset = spark.read()
                .option("header", true)
                .csv("src/main/resources/RealEstate.csv");

        dataset.createOrReplaceTempView("Estate");

        /* Create a Spark program to read the house data from in/RealEstate.csv, group by location,
        aggregate the average price per SQ Ft and max price, and sort by average price per SQ Ft. */
        Dataset<Row> asSQL = spark.sql
                ("select Location, round(avg(`Price SQ Ft`)), cast(max(Price) as int) from Estate " +
                        "group by Location order by avg(`Price SQ Ft`) desc");
        //asSQL.show();

        Dataset<Row> nonSQL = dataset
                .groupBy("Location")
                .agg(round(avg("Price SQ Ft")), round(max("Price")))
                .orderBy(avg("Price SQ Ft").desc());
        nonSQL.show();

        spark.stop();
    }
}