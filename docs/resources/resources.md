
#### Resources:
* Prime Front-End Training - https://agonlohaj.gitlab.io/prime-front-end-training/lecture/gettingStarted/
* GoalKicker, MySQL Book for Professionals - https://goalkicker.com/MySQLBook/
* W3 Schools, SQL Tutorial - https://www.w3schools.com/sql/
* Apache Spark, https://spark.apache.org/docs/2.3.3/rdd-programming-guide.html
* AWS EC2, https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/concepts.html