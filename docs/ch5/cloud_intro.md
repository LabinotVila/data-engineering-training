### AWS & Cloud Computing
***

Cloud computing is the on-demand delivery of compute power, database, storage, applications, and other IT resources via the internet with pay-as-you-go pricing.

> The availability of high-capacity networks, low-cost computers and storage devices as well as the widespread adoption of hardware virtualization, service-oriented architecture and autonomic and utility computing has led to growth in cloud computing.

[-] Wikipedia


![](_img/cloud-computing-solutions.png)

"Cloud computing" was popularized with Amazon.com releasing its Elastic Compute Cloud product in 2006, references to the phrase "cloud computing" appeared as early as 1996, with the first known mention in a [Compaq internal document](https://www.technologyreview.com/s/425970/who-coined-cloud-computing/).

Main players in Cloud industry are:
* Amazon Web Services
* Microsoft Azure
* GCP
* Alibaba
* Oracle
* IBM

#### AWS Cloud Provider
Amazon Web Services (AWS) is the world’s most comprehensive and broadly adopted cloud platform, offering over 165 fully featured services from data centers globally. 
Millions of customers —including the fastest-growing startups, largest enterprises, and leading government agencies—trust AWS to power their infrastructure, become more agile, and lower costs.

The AWS cloud platform offers over 165 fully featured services, including over 40 services that aren’t available anywhere else.


**AWS Global Infrastructure Map**

![](_img/aws_global_infra.png)

Amazon cloud services are hosted in multiple locations world-wide. These locations are composed of Regions and Availability Zones. Each Region is a separate geographic area. 

Each Region has multiple, isolated locations known as Availability Zones. Amazon EC2 provides you the ability to place resources, such as instances, and data in multiple locations. 
Resources aren't replicated across Regions unless you do so specifically.


The following table lists the Regions provided by an AWS account:

<img height="600" width="300" src="_img/region_list.png" />

<img height="400" width="400" src="_img/aws_regions.png" />

#### What Is Amazon EC2?


Amazon Elastic Compute Cloud (Amazon EC2) provides **scalable computing capacity** in the Amazon Web Services (AWS) cloud.

**You can use Amazon EC2 to launch as many or as few virtual servers as you need, configure security and networking, and manage storage.**

<img height="400" width="600" src="_img/ec2_1.png" />


**Features of Amazon EC2**

Amazon EC2 provides the following features:
* Virtual computing environments, known as instances
* Preconfigured templates for your instances, known as Amazon Machine Images (AMIs), that package the bits you need for your server (including the operating system and additional software)
* Various configurations of CPU, memory, storage, and networking capacity for your instances, known as instance types
* Secure login information for your instances using key pairs (AWS stores the public key, and you store the private key in a secure place)
* Storage volumes for temporary data that's deleted when you stop or terminate your instance, known as instance store volumes
* Persistent storage volumes for your data using Amazon Elastic Block Store (Amazon EBS), known as Amazon EBS volumes
* Multiple physical locations for your resources, such as instances and Amazon EBS volumes, known as Regions and Availability Zones
* A firewall that enables you to specify the protocols, ports, and source IP ranges that can reach your instances using security groups
* Static IPv4 addresses for dynamic cloud computing, known as Elastic IP addresses
* Metadata, known as tags, that you can create and assign to your Amazon EC2 resources
* Virtual networks you can create that are logically isolated from the rest of the AWS cloud, and that you can optionally connect to your own network, known as virtual private clouds (VPCs)


**Instances and AMIs**

An instance is a virtual server in the cloud. Its configuration at launch is a copy of the AMI that you specified when you launched the instance.

An Amazon Machine Image (AMI) is a template that contains a software configuration (for example, an operating system, an application server, and applications).

From an AMI, you launch an instance, which is a copy of the AMI running as a virtual server in the cloud. You can launch multiple instances of an AMI, as shown in the following figure.

![](_img/architecture_ami_instance.png)

AMI Types can be Linux or Windows based images.


**Instance Types**

When you launch an instance, the instance type that you specify determines the hardware of the host computer used for your instance.

Each instance type offers different compute, memory, and storage capabilities and are grouped in instance families based on these capabilities. 

AWS EC2 provides a wide range of [instance types](https://aws.amazon.com/ec2/instance-types/), you can choose the type based on the workload of your application.


**Instance Purchasing Options**

Amazon EC2 provides the following purchasing options to enable you to optimize your costs based on your needs:
* On-Demand Instances – Pay, by the second, for the instances that you launch.
* Reserved Instances – Purchase, at a significant discount, instances that are always available, for a term from one to three years.
* Scheduled Instances – Purchase instances that are always available on the specified recurring schedule, for a one-year term.
* Spot Instances – Request unused EC2 instances, which can lower your Amazon EC2 costs significantly.
* Dedicated Hosts – Pay for a physical host that is fully dedicated to running your instances, and bring your existing per-socket, per-core, or per-VM software licenses to reduce costs.
* Dedicated Instances – Pay, by the hour, for instances that run on single-tenant hardware.
* Capacity Reservations – Reserve capacity for your EC2 instances in a specific Availability Zone for any duration.


**Storage**

Amazon EC2 provides you with flexible, cost effective, and easy-to-use data storage options for your instances. It refers to the hard-disk capacity. 

These storage options include the following:
* Amazon Elastic Block Store
* Amazon EC2 Instance Store
* Amazon Elastic File System (Amazon EFS)
* Amazon Simple Storage Service (Amazon S3)


<img height="400" width="500" src="_img/architecture_storage.png" />


* **Amazon EBS**

Amazon EBS provides durable, block-level storage volumes that you can attach to a running instance. You can use Amazon EBS as a primary storage device for data that requires frequent and granular updates. For example, Amazon EBS is the recommended storage option when you run a database on an instance.

* **Amazon EC2 Instance Store**

Many instances can access storage from disks that are physically attached to the host computer. This disk storage is referred to as instance store. Instance store provides temporary block-level storage for instances. The data on an instance store volume persists only during the life of the associated instance; if you stop or terminate an instance, any data on instance store volumes is lost. For more information, see Amazon EC2 Instance Store.

* **Amazon EFS File System**

Amazon EFS provides scalable file storage for use with Amazon EC2. You can create an EFS file system and configure your instances to mount the file system. You can use an EFS file system as a common data source for workloads and applications running on multiple instances.


##### EC2 Security Group 

The rules of a security group control the inbound traffic that's allowed to reach the instances that are associated with the security group and the outbound traffic that's allowed to leave them.

<img height="300" width="400" src="_img/security_gr.png" />



##### VPC (Virtual Private Cloud)

A virtual private cloud (VPC) is a virtual network dedicated to your AWS account. It is logically isolated from other virtual networks in the AWS Cloud. You can launch your AWS resources, such as Amazon EC2 instances, into your VPC. 

Within VPC you can specify:
* IP address range for the VPC, 
* Subnets, 
* Associate Security Groups, and 
* configure Route Tables.

![](_img/default-vpc-diagram.png)

Components of a VPC:
* Subnets
* Security Groups
* Network ACLs
* Internet Gateways
* Egress Only Internet Gateways
* Route Tables
* Network Interfaces
* Peering Connections
* Endpoints

***

**Exercise 1:** Hands-on lab with AWS EC2 service.