
### Planning and Design
***

> Database designing is crucial to high performance database system.

> Apart from performance improvement, properly designed database are easy to maintain, improve data consistency and are cost effective in terms of disk storage space.


#### Database designing

* Logical model
    * This stage is concerned with developing a database model based on requirements. The entire design is on paper without any physical implementations or specific DBMS considerations.
* Physical model
    * This stage implements the logical model of the database taking into account the DBMS and physical implementation factors.
    

**Designing Techniques**

1. Normalization
2. ER Modeling


***

#### Normalization

Normalization is the process of organizing the fields and tables of a relational database to minimize <strong>redundancy</strong> and <strong>dependency</strong>

This can involve dividing larger tables into smaller tables and **defining relationships** between them. 

**Common DB Design Mistakes** 

* Tables with too many fields or with fields that do not relate to each other 
* Too many tables with similar data 
* Repeated rows 
* Using comma separated values or multiple values in a single row 
* Poor naming conventions 
* Poor or no planning 
* Non-Normalized data


**Objective**

Isolate data so that actions in a field can be made in one table and then propagated through the rest of the needed tables using **properly defined relationships**.

Types of Normalizations:

* First Normal Form (1NF)
    * No repeating or duplicate fields 
    * Each row should contain only one value 
    * Each row/record should be unique and identified by a primary key
    
    ![](_img/1nf.png)

* Second Normal Form (2NF)
    * Should be in 1NF 
    * All non-key fields depend on all components of the primary key 
    * No partial dependencies
    
    ![](_img/2nf.png)

* Third Normal Form (3RD)
    * Should be in 2NF 
    * Has no transitive functional dependencies
        > A transitive functional dependency is when changing a non-key column, might cause any of the other non-key columns to change.
    
    ![](_img/3nf.png)


**Functional Dependency**
 
* Describes a relationship between columns within a single relation. 

* A column is dependent on another if one value can be used to determine the value of another. 

Example: `first_name` is functionally dependent on `id` because `id` can be used to uniquely determine the value of first name.

