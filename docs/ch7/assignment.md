#### Home Assignment
***

1. Create a program which loads the Maze map from file `.../resources/maze_map.txt`. 
   * This loaded maze is a 2D array, consisting of:
        * Number `0` representing the path
        * Number `1` representing the wall
        * Number `2` representing the starting point
        * Number `3` representing the exit point
   * To find the path use Depth-first Search graph algorithm and implement it in a recursive fashion
   * Start your depth-first search recursion from the starting points, the position of the number `2` in the Maze map
   * Once you find the exit point (Number `3`) in the map, exit your application.

2. Read instructions below to implement the task:
    * **Part 1:**
    ![](_img/assign_1.png)
    * **Part 2:**
    ![](_img/assign_2.png)
