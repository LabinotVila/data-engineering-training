## Introduction and Project Setup


The purpose of the lecture one is to on-board the participants, get them acquainted with the way of working, the program plan, the tools that they need to setup and the training program structure.

### Getting Started
***

Let's start by installing tools that we're going to use during the traning:
* [Git](#git)
* [Gitlab](#gitlab)
* [Source Tree](#source-tree)
* [Slack](#slack)
* [IntelliJ IDEA](#intellij-idea)
* [Apache Maven](#apache-maven)
* [MySQL Workbench](#mysql-workbench)


These are all free to use tools, that’s why we choose those. Here is a quick intro to all of them

#### Git
Git is a free open source distributed version control system designed to handle everything from small to very large projects with speed and efficiency. Using GIT we will setup working branches and maintain the code changes and pushes to your working repository which contains the interactive web app with your assignments.

#### GitLab
GitLab is a single application for the entire software development lifecycle. From project planning and source code management to CI/CD, monitoring, and security. 

Using it we will publish the training source code at the repository, organise the Continuous Integration and Continuous Delivery (you will learn this later). 

We will provide the master repository, and you will fork it (you will learn this later) and work on your version of the web app (fill in the assignments). The code review (assignments review), progress tracking, changes and training timeline in the perspective of a single applicant will be all managed using GitLab.

Go ahead, follow this link: https://gitlab.com and sign up to create your first account! You are going to be using this A lot!

#### Source Tree

SourceTree is a software that has a GIT Graphical User Interface (GUI) of your repositories. It basically is a visual representation of what is going on with your code, what changes you did, and show the history of the versions of the code changes you made throughout your training. In other words, it will visually show your codding footprint! It makes doing git commands (you’ll learn them quickly) very easy, so that you can keep your focus on what's important, getting stuff done!

#### Slack
We are going to use Slack as our communication tool.
The workspace directory is located at https://primedataengtraining.slack.com
If you want to download it for your local machine use this link: https://slack.com/download. Its also available on Mobile (iOS and Android), look them up at the app store

#### IntelliJ IDEA
IntelliJ IDEA is a Java integrated development environment for developing computer software. It is developed by JetBrains, and is available as an Apache 2 Licensed community edition. You can download it from this link: https://www.jetbrains.com/idea/download/. 
When downloading ItelliJ, pick version JBR 8.

#### Apache Maven
Apache Maven is a tool that is used for building and managing any Java-based application. 

#### MySQL Workbench
MySQL Workbench provides DBAs and developers an integrated tools environment for:

* Database Design & Modeling
* SQL Development
* Database Administration
* Database Migration


<!--BREAK-->
