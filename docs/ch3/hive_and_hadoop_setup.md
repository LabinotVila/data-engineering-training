### Hadoop and Hive with MySQL Setup!
***

**Resources**

* [Install Ubuntu](https://tutorials.ubuntu.com/tutorial/tutorial-ubuntu-on-windows) as a subsystem in your Windows 10 machine
* [Install Hadoop](https://medium.com/devilsadvocatediwakar/installing-hadoop-on-ubuntu-9493af1af12f) on Ubuntu
* [Install Hive](https://medium.com/@maheshdeshmukh22/step-by-step-guide-on-how-to-install-hive-on-ubuntu-aeb9a72e950d) on Ubuntu
* [Configure Hive & Mysql](https://www.guru99.com/installation-configuration-hive-mysql.html) on Ubuntu

**Installing MySQL**

Run the following command to get the mysql-server:
```
sudo apt-get install mysql-server
```
Now run it:
```
sudo service mysql start
```
In order to change the user password we are going to use:
```
sudo mysql
USE mysql;
UPDATE user SET plugin='mysql_native_password' WHERE User='root';
UPDATE mysql.user SET authentication_string=PASSWORD('root') where user='root';
FLUSH PRIVILEGES;
```

Test it out by running the following:
```
mysql -u root -p
```

**Installing Hadoop**

If you have installed java already make sure its using the version 1.8, to check that run:
```
java -version
update-alternatives --config java
```
If that is pointing to versions higher than 1.8, than do the following:
```
sudo apt-get autoremove default-jre
sudo apt-get autoremove default-jdk
```
Than install the right version:
```
sudo apt update
sudo apt install openjdk-8-jre
sudo apt install openjdk-8-jdk
```
Verify using the first step!

Checking if ssh is properly configured and running:
```
sudo service ssh status
```
If its not running and/or you need to start it, run the following
```
sudo service ssh --full-restart
```
Now check if its working by using:
```
ssh -v localhost
```
The *-v* is used to log the output of execution, for debugging purposes!

If you are getting an unauthorized user message, than follow the steps:
1. Check the directory you are at (current user) and store it in a text file, using:

	```
	cd ~
	pwd
	```
2. Generate new key files using

	```
	ssh-keygen -t rsa -P ""
	```
3. Run the following to switch to root user and store it in a text file, using:

	```
	sudo -s -H
	```
4. Check the directory you are at (of root user), using

	```
	cd ~
	pwd
	ssh-keygen -t rsa -P ""
	```
5. Now apply the new key at the root user using:

	```
	# Let's also add root in autherized keys file
	cat /root/.ssh/id_rsa.pub >> /root/.ssh/authorized_keys

	cat /home/agon/.ssh/id_rsa.pub >> /root/.ssh/authorized_keys

	# TO verify it run:
	cd ./.ssh
	cat authorized_keys

	# Than go back to
	cd ~
	```
	Where {1} is the directory at step 1, and {4} is the directory of step 4
6. Now exit from the root user switch:

	```
	exit

	# This command return you to your previous session (user)
	# Let's also add root in autherized keys file
	cat $USER/.ssh/id_rsa.pub >> $USER/.ssh/authorized_keys

	cat /root/.ssh/id_rsa.pub >> $USER/.ssh/authorized_keys
	```

Now get back to the first section and check installemenet!

If you are running issues into Connection closed issue from ssl, than that means the open-ssl version isn't compatible with Windows version. To fix that run the following:
```
sudo apt-get purge openssh-server
sudo apt-get install openssh-server
sudo service ssh start
```

Continue with hadoop installation by running:

```
cd ~/
sudo wget http://mirrors.sonic.net/apache/hadoop/common/hadoop-2.9.1/hadoop-2.9.1.tar.gz
sudo tar xvzf hadoop-2.9.1.tar.gz
sudo rm hadoop-2.9.1.tar.gz
sudo mv * /usr/local/hadoop
sudo chown $USER:$USER /usr/local/hadoop
```

Change the .bashrc at "~/" called .bashrc

1. Check where java is stored:

	```
	update-alternatives --config java
	```
2. Run the following comand

	```
	sudo nano .bashrc
	```

3. Add the following lines at the end, JAVA_HOME can differ on your computer

	```
	#HADOOP VARIABLES START
	export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
	export HADOOP_INSTALL=/usr/local/hadoop
	export HADOOP_HOME=/usr/local/hadoop
	export PATH=$PATH:$HADOOP_INSTALL/bin
	export PATH=$PATH:$HADOOP_INSTALL/sbin
	export HADOOP_MAPRED_HOME=$HADOOP_INSTALL
	export HADOOP_COMMON_HOME=$HADOOP_INSTALL
	export HADOOP_HDFS_HOME=$HADOOP_INSTALL
	export YARN_HOME=$HADOOP_INSTALL
	export HADOOP_COMMON_LIB_NATIVE_DIR=$HADOOP_INSTALL/lib/native
	export HADOOP_OPTS="-Djava.library.path=$HADOOP_INSTALL/lib"
	alias hstart="sudo /usr/local/hadoop/sbin/start-all.sh"
	#HADOOP VARIABLES END
	```
4. Type Control + X, it will ask you whether you want to save the changes
5. Type Y to confirm, it will ask you in what name to store it
6. Type Enter, to use the name suggested!
7. Apply it using

	```
	source ~/.bashrc
	```

Modify the file at  /usr/local/hadoop/etc/hadoop/hadoop-env.sh by doing the following:
```
cd /usr/local/hadoop/etc/hadoop/
sudo nano hadoop-env.sh
export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64 # add line
```
Modify the file at /usr/local/hadoop/etc/hadoop/core-site.xml by doing the following:
```
sudo mkdir -p /app/hadoop/tmp
sudo mkdir -p /usr/local/hadoop/logs
sudo mkdir -p /usr/local/hadoop_store/hdfs/namenode
sudo mkdir -p /usr/local/hadoop_store/hdfs/datanode

sudo chown $USER:$USER /app/hadoop/tmp
sudo chmod -R 777 /app/hadoop/tmp
sudo chown $USER:$USER /usr/local/hadoop_store
sudo chmod -R 777 /usr/local/hadoop_store

# Let's make sure we give permission to user for creating logs
sudo chown $USER:$USER /usr/local/hadoop/logs
sudo chmod -R 777 /usr/local/hadoop/logs

cd /usr/local/hadoop/etc/hadoop/
sudo nano core-site.xml

Add the following lines:
<configuration>
	<property>
		<name>hadoop.tmp.dir</name>
		<value>/app/hadoop/tmp</value>
		<description>A base for other temporary directories.</description>
	</property>
	<property>
		<name>fs.default.name</name>
		<value>hdfs://localhost:54310</value>
		<description>The name of the default file system. A URI whose
		scheme and authority determine the FileSystem implementation. The
		uri’s scheme determines the config property (fs.SCHEME.impl) naming
		the FileSystem implementation class. The uri’s authority is used to
		determine the host, port, etc. for a filesystem.</description>
	</property>
</configuration>
```
Modify the file at /usr/local/hadoop/etc/hadoop/mapred-site.xml by doing the following:
```
cd /usr/local/hadoop/etc/hadoop/
sudo cp mapred-site.xml.template mapred-site.xml
sudo nano mapred-site.xml
Add the following lines:
<configuration>
        <property>
                <name>mapred.job.tracker</name>
                <value>localhost:54311</value>
                <description>The host and port that the MapReduce job tracker runs
                at. If “local”, then jobs are run in-process as a single map
                and reduce task.
                </description>
        </property>
</configuration>
```
Modify the file at /usr/local/hadoop/etc/hadoop/hdfs-site.xml by doing the following:
```
cd /usr/local/hadoop/etc/hadoop/
sudo nano hdfs-site.xml
Add the following lines:
<configuration>
        <property>
                <name>dfs.replication</name>
                <value>1</value>
                <description>Default block replication.
                The actual number of replications can be specified when the file is created.
                The default is used if replication is not specified in create time.
                </description>
        </property>
        <property>
                <name>dfs.namenode.name.dir</name>
                <value>file:/usr/local/hadoop_store/hdfs/namenode</value>
        </property>
        <property>
                <name>dfs.datanode.data.dir</name>
                <value>file:/usr/local/hadoop_store/hdfs/datanode</value>
        </property>
</configuration>
```

Now format the new hadoop file system using:
```
cd /usr/local/hadoop_store/hdfs/namenode
hadoop namenode -format
```

If all went well now you can start hadoop by running:
```
hstart
```

**Installing Hive**

Start by downloading hive:
```
wget http://www-us.apache.org/dist/hive/hive-2.3.5/apache-hive-2.3.5-bin.tar.gz
tar xvzf apache-hive-2.3.5-bin.tar.gz
rm apache-hive-2.3.5-bin.tar.gz
sudo mv apache-hive-2.3.5-bin /usr/local/
```

Than prepare some directories for hive:
```
hdfs dfs -mkdir /user/
hdfs dfs -mkdir /user/hive/
hdfs dfs -mkdir /user/hive/warehouse

hdfs dfs -mkdir /tmp/
hdfs dfs -ls /user/hive/

hdfs dfs -chmod g+w /tmp
hdfs dfs -chmod g+w /user/hive/
```

Install the mysql connector for hive:
```
sudo apt install libmysql-java
sudo ln -s /usr/share/java/mysql-connector-java.jar $HIVE_HOME/lib/mysql-connector-java.jar
```

Change the bash profile located at "~/" called .bashrc

1. Run the following comand

	```
	sudo nano .bashrc
	```

2. Add the following lines at the end

	```
	#HIVE VARIABLES START
	export HIVE_HOME=/usr/local/apache-hive-2.3.5-bin
	export HIVE_CONFIG_DIR=/usr/local/apache-hive-2.3.5-bin/conf
	export PATH=$HIVE_HOME/bin:$PATH
	export CLASSPATH=$CLASSPATH:/usr/local/hadoop/lib/*:.
	export CLASSPATH=$CLASSPATH:/usr/local/apache-hive-2.3.5-bin/lib/*:.
	#HIVE Variables END
	```
3. Type Control + X, it will ask you whether you want to save the changes
4. Type Y to confirm, it will ask you in what name to store it
5. Type Enter, to use the name suggested!
6. Apply it using

	```
	source ~/.bashrc
	```

Now lets configure hive like follows:
```
cd $HIVE_HOME
cd conf
sudo cp hive-env.sh.template hive-env.sh
sudo nano hive-env.sh
HADOOP_HOME=/usr/local/hadoop
sudo cp hive-default.xml.template hive-site.xml
```

By modifying hive-site.xml:
```
sudo nano hive-site.xml
<configuration>
	<property>
		<name>hive.exec.local.scratchdir</name>
		<value>tmp/scratch/</value>
		<description>Local scratch space for Hive jobs</description>
	</property>
	<property>
		<name>javax.jdo.option.ConnectionURL</name>
		<value>jdbc:mysql://localhost/metastore?createDatabaseIfNotExist=true</value>
		<description>metadata is stored in a MySQL server</description>
	</property>
	<property>
		<name>javax.jdo.option.ConnectionDriverName</name>
		<value>com.mysql.jdbc.Driver</value>
		<description>MySQL JDBC driver class</description>
	</property>
	<property>
		<name>javax.jdo.option.ConnectionUserName</name>
		<value>root</value>
		<description>user name for connecting to mysql server</description>
	</property>
	<property>
		<name>javax.jdo.option.ConnectionPassword</name>
		<value>root</value>
		<description>password for connecting to mysql server</description>
	</property>
</configuration>
```
replace "${system:java.io.tmpdir}" with /tmp
replace "${system:user.name}" with /hive

Now you should be able to run Hive like follows:
```
cd $HIVE_HOME
cd bin
schematool -dbType mysql -initSchema
cd ~
hive
```