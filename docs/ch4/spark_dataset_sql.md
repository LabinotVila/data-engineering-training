### Spark SQL and Datasets/DataFrames
***

Spark SQL is a Spark module for structured data processing. Unlike the basic Spark RDD API, the interfaces provided by Spark SQL provide Spark with more information about the structure of both the data and the computation being performed. 

Internally, Spark SQL uses this extra information to perform extra optimizations. There are several ways to interact with Spark SQL including SQL and the Dataset API. 

When computing a result the same execution engine is used, independent of which API/language you are using to express the computation. This unification means that developers can easily switch back and forth between different APIs based on which provides the most natural way to express a given transformation.

#### SQL

One use of Spark SQL is to execute SQL queries. Spark SQL can also be used to read data from an existing Hive installation.
When running SQL from within another programming language the results will be returned as a Dataset/DataFrame.


#### Datasets and DataFrames

A Dataset is a distributed collection of data. Dataset is a new interface added in Spark 1.6 that provides the benefits of RDDs (strong typing, ability to use powerful lambda functions) with the benefits of Spark SQL’s optimized execution engine.

The Dataset API is available in Scala and Java. Python does not have the support for the Dataset API.

A DataFrame is a Dataset organized into named columns. It is conceptually equivalent to a table in a relational database or a data frame in R/Python, but with richer optimizations under the hood

In Java API, users need to use Dataset<Row> to represent a DataFrame.

#### SparkSession

The entry point into all functionality in Spark is the `SparkSession` class. To create a basic SparkSession, just use SparkSession.builder():

```java
import org.apache.spark.sql.SparkSession;

SparkSession spark = SparkSession
  .builder()
  .appName("Java Spark SQL basic example")
  .config("spark.some.config.option", "some-value")
  .getOrCreate();
```

`SparkSession` in Spark 2.0 provides builtin support for `Hive` features including the ability to write queries using HiveQL, access to Hive UDFs, and the ability to read data from Hive tables. 


#### Creating DataFrames

With a SparkSession, applications can create DataFrames from an existing RDD, from a Hive table, or from Spark data sources.

As an example, the following creates a DataFrame based on the content of a JSON file:

```java
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
....

Dataset<Row> df = spark.read().json("src/main/resources/people.json");
```

* Dataset takes on two distinct APIs characteristics: a strongly-typed API and an untyped API.
* Consider DataFrame as untyped view of a Dataset, which is a Dataset of Row where a Row is a generic untyped JVM object.

#### Row
* Row objects represent records inside dataset and are simply fixed- length arrays of fields.
* Row objects have a number of getter functions to obtain the value of each field given its index. The get method takes a column number and returns us an object type; we are responsible for casting the object to the correct type.
    
    ```java
      Object column5 = row.get(5);
    ```

* For primitive and boxed types, there is a get type method, which returns the value of that type.
   ```java
     long column1 = row.getLong(1);
     boolean column2 = row.getBoolean(2);
   ```

#### Dataset Operations

DataFrames provide a domain-specific language for structured data manipulation in Scala, Java, Python and R.

```java
// Print the schema in a tree format
df.printSchema();

// Select only the "name" column
df.select("name").show();

// Select everybody, but increment the age by 1
df.select(col("name"), col("age").plus(1)).show();

// Select people older than 21
df.filter(col("age").gt(21)).show();

// Count people by age
df.groupBy("age").count().show();

// To create Dataset<Row> using SparkSession
Dataset<Row> people = spark.read().csv("...");
Dataset<Row> department = spark.read().csv("...");

// Filter, Join and aggregate two datasets
people.filter(people.col("age").gt(30))
 .join(department, people.col("deptId").equalTo(department.col("id")))
 .groupBy(department.col("name"), people.col("gender"))
 .agg(avg(people.col("salary")), max(people.col("age")));


// Assign a value to a column bassed on a condition expression
Dataset<Row> result = students.withColumn("ExamStatus", when(col("grade").equalTo("F"), "Failed").otherwise(lit("Passed")));

```

**Exercise 1:** Read file `employees.json` as a Dataset<Row>, print the schema, find the employee with the highest salary,
also find the total salary given to all employees. Show the results in the console.
***

##### More operations by example
```java
SparkSession session = SparkSession.builder()
    .appName("StackOverFlowSurvey")
    .master("local[3]")
    .getOrCreate();

// Print 20 records of responses table
responses.show(20);

// Print the so_region and self_identification columns of gender table
responses.select(col("so_region"),  col("self_identification")).show();

// Print records where the response is from Albania
responses.filter(col("country").equalTo("Albania")).show();

// Print the count of occupations
RelationalGroupedDataset groupedDataset = responses.groupBy(col("occupation"));
groupedDataset.count().show();

// Cast the salary mid point and age mid point to integer
Dataset<Row> castedResponse = responses.withColumn(SALARY_MIDPOINT, col(SALARY_MIDPOINT).cast("integer"))
                                       .withColumn(AGE_MIDPOINT, col(AGE_MIDPOINT).cast("integer"));

// Print out casted schema
castedResponse.printSchema();

// Print records with average mid age less than 20
castedResponse.filter(col(AGE_MIDPOINT).$less(20)).show();

// Print the result by salary middle point in descending order
castedResponse.orderBy(col(SALARY_MIDPOINT ).desc()).show();

// Group by country and aggregate by average salary middle point and max age middle point
RelationalGroupedDataset datasetGroupByCountry = castedResponse.groupBy("country");
datasetGroupByCountry.agg(avg(SALARY_MIDPOINT), max(AGE_MIDPOINT)).show();


Dataset<Row> responseWithSalaryBucket = castedResponse.withColumn(
        SALARY_MIDPOINT_BUCKET, col(SALARY_MIDPOINT).divide(20000).cast("integer").multiply(20000));

// With salary bucket column
responseWithSalaryBucket.select(col(SALARY_MIDPOINT), col(SALARY_MIDPOINT_BUCKET)).show();

// Group by salary bucket
responseWithSalaryBucket.groupBy(SALARY_MIDPOINT_BUCKET).count().orderBy(col(SALARY_MIDPOINT_BUCKET)).show();
```

An example of Spark SQL-Java DSL:

```java
public class SQLDataframeExample {

    public static void main(String[] args) {

        // Instatiate Spark session object
        SparkSession spark = SparkSession.builder()
                .appName("sqlTest")
                .master("local[*]")
                .config("spark.sql.warehouse.dir", "file:///tmp/")
                .getOrCreate();

        // Create dataset from log files
        Dataset<Row> dataset = spark.read()
                .option("header", true)
                .csv("src/main/resources/logs.txt");

        dataset = dataset.select(col("level"), date_format(col("datetime"), "MMMM").alias("month"));
        
        // count records by log level
        dataset = dataset.groupBy(col("level"), col("month")).count().orderBy(col("count"));
        dataset.show();
        
        spark.stop();
    }
}
```


#### Running SQL Queries

The `sql` function on a SparkSession enables applications to run SQL queries programmatically and returns the result as a Dataset<Row>.

```java
// Register the DataFrame as a SQL temporary view
df.createOrReplaceTempView("people");

// Run SQL query
Dataset<Row> sqlDF = spark.sql("SELECT * FROM people");
sqlDF.show();

// To create a Dataset from an ArrayList
List<Row> inMemory = new ArrayList<Row>();
// add elements as Row instances
inMemory.add(RowFactory.create(null, "11-1-2019"));
inMemory.add(RowFactory.create("FATAL", "11-1-2019"));
inMemory.add(RowFactory.create("FATAL", "11-1-2019"));
inMemory.add(RowFactory.create("WARN", "12-02-2019"));
inMemory.add(RowFactory.create("INFO", "12-02-2019"));
inMemory.add(RowFactory.create("FATAL", "13-10-2019"));

// define the schema of the dataset (row)
StructField[] fields = new StructField[]{
        new StructField("level", DataTypes.StringType, false, Metadata.empty()),
        new StructField("datetime", DataTypes.StringType, false, Metadata.empty())
};
StructType schema = new StructType(fields);

// Use createDataFrame to create a Dataset using the array data and the defined schema
Dataset<Row> dataset = spark.createDataFrame(inMemory, schema);

dataset.createOrReplaceTempView("logging_table");

// Run SQL query in the table view of the in-memory array list
Dataset<Row> result = spark.sql("select level, date_format(datetime,'MMMM') as month, count(1) as total from logging_table group by level, month");

result.createOrReplaceTempView("result_table");

Dataset<Row> sum = spark.sql("select sum(total) from result_table");
sum.show();
```

**Exercise 2:** Create a Dataset by reading the file `students.csv`, using SQL statement, filter by subject 'Modern Art' and year 2007.
***

**Spark and Apache Hive example**

```java
public class HiveExample {

    public static void main(String[] args) {
        String warehouseLocation = new File("spark-warehouse").getAbsolutePath();
        SparkSession spark = SparkSession
                .builder()
                .appName("Java Spark Hive Example")
                .master("local")
                .config("hive.metastore.uri", "thrift://localhost:9083")
                .enableHiveSupport()
                .getOrCreate();

        spark.sqlContext().sql("CREATE DATABASE IF NOT EXISTS prime_tut");
        spark.sqlContext().sql("USE prime_tut");
        spark.sqlContext().sql("CREATE TABLE IF NOT EXISTS src (key INT, value STRING) USING hive");
        spark.sqlContext().sql("LOAD DATA LOCAL INPATH 'src/main/resources/key_value.txt' INTO TABLE src");

        // Queries are expressed in HiveQL
        spark.sqlContext().sql("SELECT * FROM src").show();

        // Create DS from CSV
        Dataset<Row> students = spark.read().option("header", "true").csv("src/main/resources/students.csv");

        // Write DS to hive
        students.write().mode("overwrite").saveAsTable("students");


        spark.stop();
    }
}
```