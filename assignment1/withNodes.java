import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class withNodes {
    // Reprezentimi i nje dege, permban nje vlere, nyjen e majte, nyjen e djathte dhe nyjen prind
    static class Node {
        int value = 0;
        Node left = null;
        Node right = null;
        Node parent = null;
    }


    static void printTree(Node root) {
        ArrayList<Node> queue = new ArrayList<Node>();

        // shtyp nyjen root
        System.out.println(root.value);

        // inicio nyjen e majte dhe te djahte
        Node leftNode = root.left;
        Node rightNode = root.right;

        // shto ne queue keto dy nyje
        queue.add(leftNode);
        queue.add(rightNode);

        // per derisa queue nuk eshte zbrazur
        while (!queue.isEmpty()) {
            // merr nyjen aktuale ne queue
            Node currentNode = queue.get(0);

            // nese ekziston nyja
            if (currentNode == null)
                break;

            // shfaq nyjen dhe prindin
            System.out.println(currentNode.value + ", parent: " + currentNode.parent.value);

            // inicio nyjet e rradhes
            Node nextLeft = currentNode.left;
            Node nextRight = currentNode.right;

            // hiq elementin e pare ne queue
            queue.remove(0);
            queue.add(nextLeft); // shto nyjen majtas
            queue.add(nextRight); // shto nyjen djathtas
        }
    }

    public static void main(String[] args) {
        // inicio vargun me indeks
        int[] array = new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};
        int index = 0;
        int maxIndex = array.length;

        // inicio nyjen fillestare
        Node root = new Node();
        root.parent = null;
        root.value = array[index];

        ArrayList<Node> list = new ArrayList<Node>();
        // mbush queue me nyjen fillestare
        list.add(root);

        while (!list.isEmpty()) {
            // nyja filluese, hiq
            Node parentNode = list.get(0);
            list.remove(0);

            index++;
            if (index == maxIndex)
                break;

            // shto nyjen majtas
            Node leftNode = new Node();
            leftNode.parent = parentNode;
            leftNode.value = array[index];
            parentNode.left = leftNode;

            // shto nyjen djathtas
            index++;
            if (index == maxIndex)
                break;

            Node rightNode = new Node();
            rightNode.parent = parentNode;
            rightNode.value = array[index];
            parentNode.right = rightNode;

            list.add(leftNode);
            list.add(rightNode);
        }

        printTree(root);
    }
}
