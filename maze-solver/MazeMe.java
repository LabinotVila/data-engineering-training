import java.io.File;
import java.util.Scanner;

public class MazeMe {
    // find the maze's starting and ending coordinates
    static int startX = 0;
    static int startY = 0;

    static int endX = 0;
    static int endY = 0;

    static int[][] maze, solution;

    static int countWords (String path) {
        try {
            File file = new File(path);
            Scanner sc = new Scanner(file);
            int init = 0;
            while (sc.hasNextLine()) {
                init++;
                sc.nextLine();
            }

            return init;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return 0;
    }

    static void setUpMaze(String path) {
        try {
            File file = new File(path);
            Scanner sc = new Scanner(file);

            int till = countWords(path);

            maze = new int[till][till];
            solution = new int[till][till];

            int count = 0;
            while (sc.hasNextLine()) {
                int[] crap = new int[100];
                String[] splitted = sc.nextLine().split(" ");
                for (int i = 0; i < splitted.length; i++) {
                    crap[i] = Integer.valueOf(splitted[i]);
                }

                maze[count] = crap;
                count++;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    static void findStuff(int[][] maze) {
        for (int i = 0; i < maze.length; i++) {
            for (int j = 0; j < maze[0].length; j++) {
                if (maze[i][j] == 2) {
                    startX = i;
                    startY = j;
                }

                if (maze[i][j] == 3) {
                    endX = i;
                    endY = j;
                }
            }
        }
    }

    static boolean solveMaze(int x, int y) {
        if (x == endX && y == endY) {
            solution[x][y] = 1;
            return true;
        }

        if (x >= 0 && y >= 0 && x < 5 && y < 5 && solution[x][y] == 0 && maze[x][y] == 0) {
            solution[x][y] = 1;

            if (solveMaze(x + 1, y)) {
                return true;
            }

            if (solveMaze(x, y + 1)) {
                return true;
            }

            if (solveMaze(x - 1, y)) {
                return true;
            }

            if (solveMaze(x, y - 1)) {
                return true;
            }

            solution[x][y] = 0;
        }

        return false;
    }

    static void printMaze(int[][] maze) {
        for (int i = 0; i < maze.length; i++) {
            for (int j = 0; j < maze.length; j++) {
                System.out.print(" " + maze[i][j]);
            }
            System.out.println();
        }
    }

    public static void main(String args[])
    {
        setUpMaze("C:\\Users\\Labinot\\Desktop\\PRIME\\data-engineering-training\\src\\main\\resources\\maze_map.txt");

        System.out.println("THE GIVEN MAZE: ");
        printMaze(maze);

        findStuff(maze);
        maze[startX][startY] = 0;

        if (solveMaze(startX, startY)) {
            System.out.println("MAZE'S SOLUTION!");
            printMaze(solution);
        } else {
            System.out.println("MAZE HAS NO SOLUTION!");
        }

    }
}
